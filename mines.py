#!/usr/bin/python
import random, time
from subprocess import call
from math import ceil
mines = size = 5 #default, square maps ; by now max size allowed = 10
field = {}  #to print field[11][0] = True; ? = unknown, O = clear; True/False is has been tried
minedField = {} #secret, contains the mines; True = mine
colors = { 0:'\033[01;34m', 1:'\033[01;39m', 2:'\033[01;32m', 3:'\033[01;33m', 4:'\033[01;33m', 5:'\033[01;31m', 6:'\033[01;31m'}
clearCells = 0
 
def init():
    global size; global mines
    while True:
        aux = raw_input("Input the size of the map[negative number for default] Max = 10\n")
        if aux and int(aux) < 0: #use default value  
            break
        if aux and 0 < int(aux) <= 10:
            size = int(aux)
            if size < 3:
                print 'That\'s to small coward!'
            break
    while True:
        aux = raw_input("Amount of mines to place[negative number for default]\n")
        if aux and int(aux) < 0: #use default value
            break
        if aux:
            mines = int(aux)
            if mines > ((size**2) * 0.75):
                print 'To many mines for this size'
                continue
            break
 
    print "Total of cells : %d ; Amount of mines : %d\n\n\n" % (size**2, mines)
 
field = {}  #to print field[11][0] = True; ? = unknown, O = clear; True/False is has been tried
minedField = {} #secret, contains the mines; True = mine
 
 
def printField():
    call(['clear'])
    global field
    aux = ''
    mark = '    '
    line = '    '
    for i in range(size):
        mark = mark + str(i)+' '
        line = line+'--'
    print '\n'+mark + '\n'+line+'\n' 
    for i in range(size):
        aux = str(i)+' |    '
        for j in range(size):
            key = str(i)+str(j)
            colored = color(field[key][1])
            aux = aux + colored # to replace next line
        print aux+' | '+str(i)
        aux = ''
    print '\n'+line+'\n'+mark+'\n'
def printMinedField():
    global minedField
    for i in range(size):
        aux = ''
        for j in range(size):
            key = str(i)+str(j)
            if minedField[key]:
                aux = aux + 'X '
            else:
                aux = aux + 'O '
        print aux
        aux = ''
 
def placeMines():
    global minedField
    count = 0
    while count < mines:
        #inicializar semilla
        random.seed(time.time())
        x = random.randrange(0,size)
        random.seed(time.time())
        y = random.randrange(0,size)
        key = str(x) + str(y)
        #print key
        if False == minedField[key]:
            minedField[key] = True
            count += 1
         
 
def generateFields():
    global minedField; global field;
    for i in range(size):
        for j in range(size):
            key = str(i)+str(j)
            minedField.update( { key : False  } )
            field.update( { key : ( False, '?') } )
    placeMines()
 
 
def main():
    while True:
        init()
        generateFields()
        printField()
        print
        #printMinedField()
        while choose():
            if clearCells == size**2 - mines:
                print 'All mines found!!! YOU WIN THIS TIME'
                break
        aux = raw_input('\n\n\nAgain?[Y/N]')
        if aux and str(aux).upper() != 'Y':
            break
         
 
def choose():
    while True:
        choice = raw_input("Select the cell to check[linecolumn] : ")
        if choice and len(choice) == 2 and ( 0 <= int(choice[0]) < size  ) and ( 0 <= int(choice[1]) < size ): # if want size bigger than 10 must be control this 
            if minedField[choice]:
                print "BOOOOM!"
                return False;
            else:
                update( choice )
                printField()    
                return True
        print 'That\'s not a valid cell'
 
def update( pos ):  #if not surrounded by mines -> O & reveal sourroundings elsif there's mines touching -> number
    global field; global clearCells;
    clearCells += 1
    offset = [0,1,-1]
    mines = surroundings(pos)
    field[pos] = (True, str(mines))
    if mines == 0:
        for i in offset:
                    for j in offset:
                            x = int(pos[0])+i; y = int(pos[1])+j
                            if 0 <= x < size and 0 <= y < size:
                                    key = str(x)+str(y)
                    if field[key][0] == False:
                        update(key) 
def surroundings( pos ):
    offset = [0,-1,1]
    near = 0
    for i in offset:
        for j in offset:
            x = int(pos[0])+i; y = int(pos[1])+j
            if 0 <= x < size and 0 <= y < size:
                key = str(x)+str(y)
                if minedField[key]:
                    near += 1          
    return near
 
def color(number):
    global colors
    if number == '?':
        return '\033[01;30m' + number + '\033[m '
    else:
        return colors[int(number)] + number + '\033[m '
 
############################ MAIN ###############################
main()